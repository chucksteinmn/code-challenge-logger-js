# Requirements
Create a extendable Logger function in JavaScript. The Logger function should be configurable to log a default message and extendable to add new logging methods. The code should be unit tested.

In a code example, show how to extend the Logger by adding the following three logging methods to an instantiated Logger.

1. console.log(msg)
2. alert(msg)
3. document.write(msg)

## Workflow 

* Fork this repository 
* Create a branch to work in
* Do a pull request when your code is ready to be reviewed

Store code examples and documentation in the wiki of your repo.

All code should use spaces instead of tabs. 